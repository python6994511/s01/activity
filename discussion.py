# [ Section ] Comments;
# Comments in Python are done in using "ctrl + /";

# [ Section ] Python Syntax
# Hello World in Python
# print("Hello World!");

# [ Section ] Python Indentation
# Where in other programming languages the indention in code is for readability only, the indentation in python is very important
# In Python, indentation is usde to indicate a block of code.
# Similar to JS, there is no need to end statements with semicolon

# [ Section ] Variables
# Variables are the container of data
# In Python, a variable is declared by stating the variable name and assigning a value using the equal symbol

# [ Section ] Naming Convention
# The terminology used for variable names is identifier
# All identifers should begin with a letter (A to Z/ a to z), dollar sign or an underscore.
# Unlike Javascript that uses the camel casing, Python uses snake casing convention for variables as defined in the PEP(Python Enhancement Proposal)
# Most importantly, identifers are case sensitive
age = 35;
middle_initial = "C";

# Python allows assigning of values to multiple variables in one line

name1, name2, name3, name4 = "John", "Paul", "George", "Ringo";
# print(age, middle_initial, name1, name2, name3, name4);

# [ Section ] Data Types
# Data types convey what kind of information a cariable holds. There are different data types and each has its own use.

# In python, there are the commonly used data types:
# 1. String(str) - for alphanumeric and symbols
full_name = "123";
secret_code = "Pa$$word";

# 2. Numbers (int, float, complex) - for integers, decimal and complex numbers
num_of_days = 365; # This is an integer
pi_approx = 3.1416; # This is a float
complex_num = 1 + 5j; # This is a complex number ( letter j represents imaginary number )
# print(type(num_of_days));
# print(type(pi_approx));
# print(type(complex_num));

# 3. Boolean(bool) - for truth values
# 
isTrue = True;
isFalse = False;
# print(isTrue, isFalse);

# [ Section ] Using Variables
# Just like in JS variables are used by simply calling the name of the identifier

# [ Section ] Terminal Outputs
# In python, printing in the terminal uses the print() function
# To use variables, concatenate (+ symbol) between strings can be used
# However, we cant concatenate string to a number or to a different data type
# print("My age is " + age);

# [ Section ] Typecasting
# here are some functions that can be used in typecasting
# 1. int() - convert the value into an integer
# print(int(3.15));
# 2. float() - converts the value into an integer value
# print(float(5));
# 3. str() - converts the value into string
# print("My age is " + str(age));
# print(type(str(age)));

# Another way to avoid type error in printing without the us of typecasting
# f-strings
# print(f"Hi my name is {full_name} and my age is {age}.");

# [ Section ] Operations 
# Python has operator families that can be used to manipulate variables

# Arithmetic Operator - performs mathematical operations
print(1+10);
print(15-8);
print(18*9);
print(21/7);
print(18%4);
print(2**4);

# Assignment operator - used to assign to variables
num1 = 4;
num1 += 3;
print(num1);

# other operators -=, *=, /=, %=

# Comparison operators - used compare values (boolean values)
print(1 == "1");
print(1 == int("1"));
# other operators, !=, >=, <=, >, <

# Logical operators - used to combine conditional statements
# Logical Or operator
print(True or False);
# Logical And Operator
print(True and False);
# Logical Not Operator
print(not False);