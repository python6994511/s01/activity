# Create 5 variables and output them in the command prompt in the following format:
name = "Josh";
age = 26;
occupation = "software";
movie = "Sample Film";
rating = 80.50; 

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating} %.");

# Create 3 variables, num1, num2, and num3
num1, num2, num3 = 1, 2, 3;

# Get the product of num1 and num2
print(num1, num2);

# Check if num1 is less than num3
print(num1 < num3);

# Add the value of num3 to num2
print(num3 + num2);